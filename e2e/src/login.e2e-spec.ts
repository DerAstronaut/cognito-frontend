import {browser, protractor} from 'protractor';
import {LoginPage} from './login.po';

describe('Login page', () => {
  let page: LoginPage;
  const EC = protractor.ExpectedConditions;

  beforeEach(() => {
    page = new LoginPage();
    browser.get('/auth/signin');
    browser.wait(EC.visibilityOf(page.signIn), 5000);
  });

  it('should have correct titles and button text', () => {
    expect(page.emailaddressLabel.getText()).toContain('E-Mail Adresse');
    expect(page.passwordLabel.getText()).toContain('Passwort');
    expect(page.signIn.getText()).toEqual('Anmelden');
  });

  it ('should display an error message to the user if they provided incorrect credentials', () => {
    page.trySignIn('123', '123');
    browser.wait(EC.visibilityOf(page.errorMessage));
    expect(page.errorMessage.getText()).toEqual('Incorrect username or password');
  });

  // it ('should redirect the user to the dashboard page if they provided correct credentials', () => {
  //   const dashboardPage = new DashboardPage();
  //   page.trySignIn('correct', 'correct');
  //   browser.wait(EC.visibilityOf(dashboardPage.title));
  //   expect(dashboardPage.title.isPresent()).toBeTruthy();
  // });
});