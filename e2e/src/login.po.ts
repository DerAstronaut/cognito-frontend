import {browser, element, by} from 'protractor';

export class LoginPage {
  get emailaddressLabel() {
    return element(by.id('mat-form-field-label-1'));
  }

  get emailaddress() {
    return element(by.id('mat-input-0'));
  }

  get passwordLabel() {
    return element(by.id('mat-form-field-label-3'));
  }

  get password() {
    return element(by.id('mat-input-1'));
  }

  get errorMessage() {
    return element(by.className('login-error'));
  }

  get signIn() {
    return element(by.buttonText('Anmelden'));
  }

  trySignIn(emailaddress: string, password: string) {
    this.emailaddress.sendKeys(emailaddress);
    this.password.sendKeys(password);
    this.signIn.click();
  }
}