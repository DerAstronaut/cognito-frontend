import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { ConfirmCodeComponent } from './auth/confirm-code/confirm-code.component';
import { ProfileComponent } from './auth/profile/profile.component';
import { AuthGuard } from './auth/auth.guard';
import { PasswordResetComponent } from './auth/password-reset/request-reset/password-reset.component';
import { VerifyResetComponent } from './auth/password-reset/verify-reset/verify-reset.component';

const routes: Routes = [
	{
		path: 'auth',
		component: AuthComponent,
		children: [
			{
				path: 'signin',
				component: SignInComponent,
				canActivate: [],
			},
			{
				path: 'signup',
				component: SignUpComponent,
				canActivate: [],
			},
			{
				path: 'confirm/:email',
				component: ConfirmCodeComponent,
				canActivate: [],
			},
			{
				path: 'profile',
				component: ProfileComponent,
				canActivate: [],
			},
			{
				path: 'resetpassword',
				component: PasswordResetComponent,
				data: {
					name: 'resetpassword',
				},
			},
			{
				path: 'resetpassword/:email',
				component: PasswordResetComponent,
				data: {
					name: 'resetpassword',
				},
			},
			{
				path: 'verifypassword',
				component: VerifyResetComponent,
				data: {
					name: 'verifypassword',
				},
			},
			{
				path: 'verifypassword/:email',
				component: VerifyResetComponent,
				data: {
					name: 'verifypassword',
				},
			},
		],
	},
	{ path: 'home', loadChildren: './home/home.module#HomeModule' },
	{
		path: '',
		redirectTo: 'auth/signin',
		pathMatch: 'full',
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
