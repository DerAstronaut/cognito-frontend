import { Component, ChangeDetectorRef, EventEmitter, Output, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav, MatSnackBar } from '@angular/material';
import { IosInstallComponent } from './shared/components/ios-install/ios-install.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	constructor(translate: TranslateService) {
		const userLang = navigator.language.split('-')[0];
		translate.setDefaultLang(userLang);
		// the lang to use, if the lang isn't available, it will use the current loader to get them
		translate.use('de');
	}

	ngOnInit() {}
}
