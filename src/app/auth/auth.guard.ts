import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import Auth from '@aws-amplify/auth';
import { AmplifyService } from 'aws-amplify-angular';

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate {
	constructor(private _router: Router, private _amplifyService: AmplifyService) {}
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean> | Promise<boolean> | boolean {
		return this._amplifyService
			.auth()
			.currentAuthenticatedUser()
			.then(
				() => {
					return true;
				},
				() => {
					this._router.navigate(['auth/signin']);
					return false;
				}
			);
	}
}
