import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AuthComponent } from './auth.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ConfirmCodeComponent } from './confirm-code/confirm-code.component';
import { ProfileComponent } from './profile/profile.component';
import { AvatarComponent } from './profile/avatar/avatar.component';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './auth.service';
import { PasswordRegexService } from '../shared/services/password-regex.service';
import { VerifyResetComponent } from './password-reset/verify-reset/verify-reset.component';
import { PasswordResetComponent } from './password-reset/request-reset/password-reset.component';

@NgModule({
	declarations: [
		AuthComponent,
		SignInComponent,
		SignUpComponent,
		ConfirmCodeComponent,
		ProfileComponent,
		AvatarComponent,
		VerifyResetComponent,
		PasswordResetComponent,
	],
	// TODO: get rid of Authservice all Methods are aviable in the Amplifyservice
	providers: [AuthService, PasswordRegexService],
	imports: [CommonModule, SharedModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
})
export class AuthModule {}
