import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CognitoErrorHandleService } from 'src/app/shared/services/cognito-error-handle.service';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AmplifyService } from 'aws-amplify-angular';

@Component({
	selector: 'app-confirm-code',
	templateUrl: './confirm-code.component.html',
	styleUrls: ['./confirm-code.component.scss'],
})
export class ConfirmCodeComponent implements OnInit {
	errorMessage: string;
	successMessage: string;
	email: string;
	confirmForm: FormGroup;
	errorMessageKey: string;
	private sub: any;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private cognitoErrorHandleService: CognitoErrorHandleService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService,
		private amplifyService: AmplifyService,
		private formBuilder: FormBuilder
	) {
		this.confirmForm = this.formBuilder.group({
			email: new FormControl({ value: this.email, disabled: true }, [Validators.email]),
			code: new FormControl('', [Validators.required, Validators.min(3)]),
		});
	}

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.email = params.email;
			this.confirmForm.patchValue({ email: this.email });
		});
	}

	sendAgain() {
		this.amplifyService
			.auth()
			.resendSignUp(this.email)
			.then((data: any) => {
				this.successMessage = 'AUTH.CONFIRM_CODE_FORM.CODE_SEND_AGAIN';
				this.snackBar.open(this.translateService.instant(this.successMessage), 'OK', {
					duration: 2000,
					verticalPosition: 'top',
					panelClass: 'accent-snackbar',
				});
			})
			.catch(err => {
				this.errorMessageKey = this.cognitoErrorHandleService.translateErrorMessage(err.message);
				this.snackBar.open(this.translateService.instant(this.errorMessageKey), 'OK', {
					duration: 5000,
					verticalPosition: 'top',
					panelClass: 'error-snackbar',
				});
			});
	}

	public OnConfirmCode(formData): void {
		this.errorMessage = null;
		this.successMessage = null;
		this.amplifyService
			.auth()
			.confirmSignUp(this.email, formData.code)
			.then((data: any) => {
				console.log(data);
				this.router.navigate(['auth/signin']);
			})
			.catch(err => {
				console.log(err);
				this.errorMessageKey = this.cognitoErrorHandleService.translateErrorMessage(err.message);
				this.snackBar.open(this.translateService.instant(this.errorMessageKey), 'OK', {
					duration: 5000,
					verticalPosition: 'top',
					panelClass: 'error-snackbar',
				});
			});
	}
}
