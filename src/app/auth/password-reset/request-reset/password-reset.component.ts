import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AmplifyService } from 'aws-amplify-angular';
import { CognitoErrorHandleService } from 'src/app/shared/services/cognito-error-handle.service';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-password-reset',
	templateUrl: './password-reset.component.html',
	styleUrls: ['./password-reset.component.scss'],
})
export class PasswordResetComponent implements OnInit, OnDestroy {
	resetForm: FormGroup;
	email: string;
	errorMessage: string;
	successMessage: string;
	verificationCodeSent = false;
	private sub: any;
	options = {
		hideRequired: false,
		floatLabel: 'auto',
	};
	errorMessageKey: string;

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		public route: ActivatedRoute,
		private cognitoErrorHandleService: CognitoErrorHandleService,
		private amplifyService: AmplifyService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService
	) {
		this.resetForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.email]],
		});
	}

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.email = params.email;
			this.resetForm.patchValue({ email: this.email });
		});
		this.errorMessage = null;
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	onForgotPassword(user): void {
		this.errorMessage = null;
		this.amplifyService
			.auth()
			.forgotPassword(user.email)
			.then(data => {
				this.verificationCodeSent = true;
				this.successMessage = 'AUTH.RESET_PASSWORD_FORM.CHECK_VERIFICATION_MAIL';
				this.snackBar.open(this.translateService.instant(this.successMessage), 'OK', {
					duration: 2000,
					verticalPosition: 'top',
					panelClass: 'accent-snackbar',
				});
				setTimeout(() => {
					this.router.navigate(['/auth/verifypassword', user.email]);
				}, 2000);
			})
			.catch(err => {
				console.log('Password Reset Request error: ', err);
				console.log(this.cognitoErrorHandleService.translateErrorMessage(err.message));
				this.errorMessageKey = this.cognitoErrorHandleService.translateErrorMessage(err.message);
				this.snackBar.open(this.translateService.instant(this.errorMessageKey), 'OK', {
					duration: 5000,
					verticalPosition: 'top',
					panelClass: 'error-snackbar',
				});
			});
	}
}
