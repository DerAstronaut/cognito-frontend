import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyResetComponent } from './verify-reset.component';

describe('RegisterComponent', () => {
	let component: VerifyResetComponent;
	let fixture: ComponentFixture<VerifyResetComponent>;

	beforeEach(
		async(() => {
			TestBed.configureTestingModule({
				declarations: [VerifyResetComponent]
			}).compileComponents();
		})
	);

	beforeEach(() => {
		fixture = TestBed.createComponent(VerifyResetComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
