import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AmplifyService } from 'aws-amplify-angular';
import { CognitoErrorHandleService } from 'src/app/shared/services/cognito-error-handle.service';
import { PasswordRegexService } from 'src/app/shared/services/password-regex.service';
import { RegexValidator } from 'src/app/shared/validators/regex-validator';
import { PasswordValidation } from 'src/app/shared/validators/password-validator';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-verify-reset',
	templateUrl: './verify-reset.component.html',
	styleUrls: ['./verify-reset.component.scss'],
})
export class VerifyResetComponent implements OnInit, OnDestroy {
	errorMessage: string;
	hide = true;
	successMessage: string;
	email: string;

	private sub;
	verifypasswordForm: FormGroup;

	options = {
		hideRequired: false,
		floatLabel: 'auto',
	};
	errorMessageKey: string;

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		public route: ActivatedRoute,
		private cognitoErrorHandleService: CognitoErrorHandleService,
		private amplifyService: AmplifyService,
		private passwordRegexService: PasswordRegexService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService
	) {
		this.verifypasswordForm = this.formBuilder.group(
			{
				email: ['', [Validators.required, Validators.email]],
				verificationCode: ['', Validators.required],
				password: [
					'',
					[
						Validators.required,
						Validators.minLength(8),
						RegexValidator.regexValidator(this.passwordRegexService.regex, { regexerror: 'regexerror' }),
						RegexValidator.regexValidator(this.passwordRegexService.numberRegex, { numbererror: 'numbererror' }),
						RegexValidator.regexValidator(this.passwordRegexService.lowerCaseRegex, { lowercase: 'lowercase' }),
						RegexValidator.regexValidator(this.passwordRegexService.upperCaseRegex, { uppercase: 'uppercase' }),
						RegexValidator.regexValidator(this.passwordRegexService.specialCharacterRegex, {
							specialcharacter: 'specialcharacter',
						}),
					],
				],
				confirmedPassword: ['', [Validators.required]],
			},
			{
				// you can only use this validator if your fields have the name password and confirmedPassword
				validator: PasswordValidation.MatchPassword,
			}
		);
	}

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.email = params.email;
			this.verifypasswordForm.patchValue({ email: this.email });
		});
		this.errorMessage = null;
	}
	ngOnDestroy() {
		this.sub.unsubscribe();
	}
	public onResetPassword(formData): void {
		this.errorMessage = null;
		this.successMessage = null;
		this.amplifyService
			.auth()
			.forgotPasswordSubmit(formData.email, formData.verificationCode, formData.password)
			.then((cognitoResponse: any) => {
				this.successMessage = 'AUTH.VERIFY_PASSWORD_FORM.PASSWORD_CHANGE_SUCCESS';
				this.snackBar.open(this.translateService.instant(this.successMessage), 'OK', {
					duration: 2000,
					verticalPosition: 'top',
					panelClass: 'accent-snackbar',
				});
				setTimeout(() => {
					this.router.navigate(['/auth/signin']);
				}, 2000);
			})
			.catch(err => {
				this.errorMessageKey = this.cognitoErrorHandleService.translateErrorMessage(err.message);
				this.snackBar.open(this.translateService.instant(this.errorMessageKey), 'OK', {
					duration: 5000,
					verticalPosition: 'top',
					panelClass: 'error-snackbar',
				});
			});
	}
}
