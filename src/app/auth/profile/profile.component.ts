import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CognitoUser } from '@aws-amplify/auth';
import { NotificationService } from 'src/app/services/notification.service';
import { LoaderService } from 'src/app/shared/components/loader/loader.service';
import { CognitoErrorHandleService } from 'src/app/shared/services/cognito-error-handle.service';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AmplifyService } from 'aws-amplify-angular';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
	profileForm: FormGroup = new FormGroup({
		email: new FormControl('', [Validators.email]),
		phone: new FormControl('', [Validators.min(10)]),
		fname: new FormControl('', [Validators.min(2)]),
		lname: new FormControl('', [Validators.min(2)]),
	});
	currentAvatarUrl: string;
	avatar: string;
	deleteAvatar = false;
	profile: any = {};
	user: CognitoUser;
	errorMessageKey: string;

	get emailInput() {
		return this.profileForm.get('email');
	}
	get fnameInput() {
		return this.profileForm.get('fname');
	}
	get lnameInput() {
		return this.profileForm.get('lname');
	}
	get phoneInput() {
		return this.profileForm.get('phone');
	}

	constructor(
		private _authService: AuthService,
		private _router: Router,
		private _notification: NotificationService,
		public loading: LoaderService,
		private cognitoErrorHandler: CognitoErrorHandleService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService,
		private amplifyService: AmplifyService
	) {}

	ngOnInit() {
		this.loading.show();
		this.getUserInfo();
	}

	async getUserInfo() {
		this.profile = await this.amplifyService.auth().currentUserInfo();
		this.user = await this.amplifyService.auth().currentAuthenticatedUser();
		if (this.profile.attributes.profile) {
			this.avatar = this.profile.attributes.profile;
			this.currentAvatarUrl = (await this.amplifyService.storage().vault.get(this.avatar)) as string;
		}
		this.fnameInput.setValue(this.profile.attributes.given_name);
		this.lnameInput.setValue(this.profile.attributes.family_name);
		this.phoneInput.setValue(this.profile.attributes.phone_number);
		this.loading.hide();
	}

	getEmailInputError() {
		if (this.emailInput.hasError('email')) {
			return 'Please enter a valid email address.';
		}
		if (this.emailInput.hasError('required')) {
			return 'An Email is required.';
		}
	}

	signOut() {
		this.amplifyService
			.auth()
			.signOut()
			.then(() => this._router.navigate(['auth/signin']));
	}

	onAvatarUploadComplete(data: any) {
		console.log(data);
		this.avatar = data.key;
		this.loading.hide();
	}

	onAvatarUpload(event) {}

	onAvatarRemove() {
		this.avatar = undefined;
		this.currentAvatarUrl = undefined;
		this.deleteAvatar = true;
	}

	async editProfile() {
		try {
			const attributes = {
				given_name: this.fnameInput.value,
				family_name: this.lnameInput.value,
				phone_number: this.phoneInput.value,
				profile: null,
			};
			if (this.avatar) {
				attributes.profile = this.avatar;
			}
			await this.amplifyService.auth().updateUserAttributes(this.user, attributes);
			if (!this.avatar && this.deleteAvatar) {
				this.user.deleteAttributes(['profile'], error => {
					if (error) {
						console.log(this.cognitoErrorHandler.translateErrorMessage(error.message));
						console.log(error);
					}
					this._notification.show('Your profile information has been updated.');
				});
			} else {
				this._notification.show('Your profile information has been updated.');
			}
		} catch (error) {
			console.log(this.cognitoErrorHandler.translateErrorMessage(error.message));
			this.errorMessageKey = this.cognitoErrorHandler.translateErrorMessage(error.message);
			this.snackBar.open(this.translateService.instant(this.errorMessageKey), 'OK', {
				duration: 5000,
				verticalPosition: 'top',
				panelClass: 'error-snackbar',
			});
			console.log(error);
		}
	}
}
