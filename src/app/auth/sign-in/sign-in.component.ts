import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { CognitoUser } from '@aws-amplify/auth';
import { NotificationService } from 'src/app/services/notification.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/shared/components/loader/loader.service';
import { CognitoErrorHandleService } from 'src/app/shared/services/cognito-error-handle.service';
import { AmplifyService } from 'aws-amplify-angular';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {
	signinForm: FormGroup = new FormGroup({
		email: new FormControl('', [Validators.email, Validators.required]),
		password: new FormControl('', [Validators.required, Validators.min(6)]),
	});

	hide = true;
	errorMessageKey: string;
	constructor(
		public auth: AuthService,
		private _notification: NotificationService,
		private _router: Router,
		private _loader: LoaderService,
		private cognitoErrorHandler: CognitoErrorHandleService,
		private amplifyService: AmplifyService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService
	) {}

	public OnSignIn(formData): void {
		this._loader.show();
		this.amplifyService
			.auth()
			.signIn(formData.email, formData.password)
			.then(
				(user: CognitoUser | any) => {
					this._loader.hide();
					this._router.navigate(['/home']);
				},
				error => {
					this.errorMessageKey = this.cognitoErrorHandler.translateErrorMessage(error.message);
					this.translateService.get(this.errorMessageKey).subscribe(translation => {
						this.snackBar.open(translation, 'OK', {
							duration: 5000,
							verticalPosition: 'top',
							panelClass: 'error-snackbar',
						});
					});
					console.log(error.message);
					this._loader.hide();
					switch (error.code) {
						case 'UserNotConfirmedException':
							this._router.navigate(['auth/confirm']);
							break;
						case 'UsernameExistsException':
							this._router.navigate(['auth/signin']);
							break;
					}
				}
			);
	}
}
