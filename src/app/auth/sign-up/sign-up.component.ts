import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatBottomSheet } from '@angular/material';
import { AuthService } from '../auth.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AmplifyService } from 'aws-amplify-angular';
import { CognitoErrorHandleService } from 'src/app/shared/services/cognito-error-handle.service';
import { RegexValidator } from 'src/app/shared/validators/regex-validator';
import { PasswordRegexService } from 'src/app/shared/services/password-regex.service';
import { PasswordValidation } from 'src/app/shared/validators/password-validator';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-sign-up',
	templateUrl: './sign-up.component.html',
	styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
	hide = true;
	signupForm: FormGroup;

	/**
	 * The key to use in the Translationservice/template usually returned from another service
	 */
	errorMessageKey: string;

	constructor(
		private _bottomSheet: MatBottomSheet,
		private _authService: AuthService,
		private _router: Router,
		private _amplifyService: AmplifyService,
		private cognitoErrorHandler: CognitoErrorHandleService,
		private formBuilder: FormBuilder,
		private passwordRegexService: PasswordRegexService,
		private snackBar: MatSnackBar,
		private translateService: TranslateService
	) {
		this.signupForm = formBuilder.group(
			{
				email: new FormControl('', [Validators.email, Validators.required]),
				password: [
					'',
					[
						Validators.required,
						Validators.minLength(8),
						RegexValidator.regexValidator(this.passwordRegexService.regex, { regexerror: 'regexerror' }),
						RegexValidator.regexValidator(this.passwordRegexService.numberRegex, { numbererror: 'numbererror' }),
						RegexValidator.regexValidator(this.passwordRegexService.lowerCaseRegex, { lowercase: 'lowercase' }),
						RegexValidator.regexValidator(this.passwordRegexService.upperCaseRegex, { uppercase: 'uppercase' }),
						RegexValidator.regexValidator(this.passwordRegexService.specialCharacterRegex, {
							specialcharacter: 'specialcharacter',
						}),
					],
				],
				confirmedPassword: ['', [Validators.required]],
				phone: new FormControl('', [Validators.min(10)]),
				fname: new FormControl('', [Validators.min(2)]),
				lname: new FormControl('', [Validators.min(2)]),
			},
			{
				// you can only use this validator if your fields have the name password and confirmedPassword
				validator: PasswordValidation.MatchPassword,
			}
		);
	}

	ngOnInit() {}

	public OnSignUp(formData) : void {
		console.log(formData.phone);
		this._amplifyService
			.auth()
			.signUp({
				username: formData.email,
				password: formData.password,
				attributes: {
					email: formData.email,
					given_name: formData.fname,
					family_name: formData.lname,
					phone_number: formData.phone,
				},
			})
			.then(data => {
				this._router.navigate(['auth/confirm', formData.email]);
			})
			.catch(error => {
				this.errorMessageKey = this.cognitoErrorHandler.translateErrorMessage(error.message);
				this.snackBar.open(this.translateService.instant(this.errorMessageKey), 'OK', {
					duration: 5000,
					verticalPosition: 'top',
					panelClass: 'error-snackbar',
				});
				console.log(error);
			});
	}
}
