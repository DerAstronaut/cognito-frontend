import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDatepicker } from '@angular/material';
import { AmplifyService } from 'aws-amplify-angular';
import { scan, mergeMap } from 'rxjs/operators';
import { Observable, Subject, BehaviorSubject, EMPTY } from 'rxjs';
import { addDays, subDays, setDate, addMonths, subMonths, startOfMonth } from 'date-fns';
import { S3 } from 'aws-sdk/clients/all';
import { AWS } from '@aws-amplify/core';
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
	@ViewChild('picker') datePicker: MatDatepicker<any>;
	constructor(private amplifyService: AmplifyService) {}

	ngOnInit() {
		this.amplifyService
			.auth()
			.currentAuthenticatedUser()
			.then(user => {
				console.log(user);

				this.amplifyService
					.auth()
					.currentCredentials()
					.then(creds => {
						AWS.config.credentials = creds;
						console.log(creds);
						const s3 = new S3({ apiVersion: '2006-03-01', region: 'eu-central-1' });
						const params = {
							Bucket: 'amplify-starter-dev-20190628194434-deployment',
						};
						console.log(AWS.config.credentials);
						s3.listObjects(params, (err, data) => {
							if (err) {
								console.log(err, err.stack);
							}
							// an error occurred
							else {
								console.log(data);
							} // successful response
						});
					});
			});
	}
}
