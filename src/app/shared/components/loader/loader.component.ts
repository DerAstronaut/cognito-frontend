import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-loader',
	templateUrl: './loader.component.html',
	styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
	message;
	tranlatedMessage;

	constructor(@Inject(MAT_DIALOG_DATA) public data: any, private translateService: TranslateService) {
		if (data.message) {
			this.message = data.message;
		}
	}

	ngOnInit() {
		this.translateService.get(this.message).subscribe(
			label => {
				this.tranlatedMessage = label;
				console.log(this.tranlatedMessage);
			},
			err => {
				console.log(err);
			}
		);
		console.log(this.tranlatedMessage);
	}
}
