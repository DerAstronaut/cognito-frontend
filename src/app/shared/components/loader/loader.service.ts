import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoaderComponent } from './loader.component';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
	providedIn: 'root',
})
export class LoaderService {
	loading: boolean;
	dialogRef: MatDialogRef<LoaderComponent>;

	defaultMessage = 'LOADER.LABEL';

	constructor(private _dialog: MatDialog, private translateService: TranslateService) {}

	show(message?): void {
		if (message === undefined) {
			message = this.defaultMessage;
		}
		this.loading = true;
		this.dialogRef = this._dialog.open(LoaderComponent, {
			width: '80%',
			data: { message },
			closeOnNavigation: false,
		});
		console.log(this.dialogRef);
		this.dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed', result);
		});
	}

	hide() {
		console.log(this.dialogRef);
		this.loading = false;
		this.dialogRef.close();
		this._dialog.closeAll();
	}
}
