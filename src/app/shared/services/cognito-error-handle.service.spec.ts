import { TestBed } from '@angular/core/testing';

import { CognitoErrorHandleService } from './cognito-error-handle.service';

describe('CognitoErrorHandleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CognitoErrorHandleService = TestBed.get(CognitoErrorHandleService);
    expect(service).toBeTruthy();
  });
});
