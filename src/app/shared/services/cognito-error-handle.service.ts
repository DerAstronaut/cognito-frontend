import { Injectable } from '@angular/core';

export interface CognitoErrorTranslation {
	errorMessage: string;
	errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier;
}

export enum CognitoErrorTypeTranslationIdentifier {
	ACCOUNT_ALREADY_EXISTS = 'AUTH.ERROR.ACCOUNT_ALREADY_EXISTS',
	USER_UNKNOWN = 'AUTH.ERROR.USER_DOES_NOT_EXIST',
	INCORRECT_USERNAME_OR_PASSWORD = 'AUTH.ERROR.INCORRECT_USERNAME_OR_PASSWORD',
	RESET_PASSWORD_GENERAL_ERROR = 'AUTH.ERROR.GENERAL_ERROR',
	RESET_PASSWORD_REQUIRED = 'AUTH.ERROR.GENERAL_ERROR',
	LIMIT_EXCEEDED = 'AUTH.ERROR.LIMIT_EXCEEDED',
	PHONE_NUMBER_FORMAT = 'AUTH.ERROR.PHONE_NUMBER_FORMAT',
	REQUEST_CODE_AGAIN = 'AUTH.ERROR.REQUEST_CODE_AGAIN',
}

export enum CognitoErrorTypes {
	RESET_PASSWORD_REQUIRED = 'PasswordResetRequiredException',
	LIMIT_EXCEEDED = 'LimitExceededException',
}

export interface CognitoError {
	code: string;
	message: string;
	name: string;
}
/**
 * This Class has the sole purpose of mapping all Cognito Error Messages to identifiers so we can use them in our translations
 * @export
 */
@Injectable()
export class CognitoErrorHandleService {
	private errorTranslations: CognitoErrorTranslation[] = [
		{
			errorMessage: 'An account with the given email already exists.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.ACCOUNT_ALREADY_EXISTS,
		},
		{
			errorMessage: 'User does not exist.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.USER_UNKNOWN,
		},
		{
			errorMessage: 'Incorrect username or password.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.INCORRECT_USERNAME_OR_PASSWORD,
		},
		{
			errorMessage: 'Username/client id combination not found.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.RESET_PASSWORD_GENERAL_ERROR,
		},
		{
			errorMessage: 'Password reset required for the user',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.RESET_PASSWORD_REQUIRED,
		},
		{
			errorMessage: 'Attempt limit exceeded, please try after some time.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.LIMIT_EXCEEDED,
		},
		{
			errorMessage: 'Invalid phone number format.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.PHONE_NUMBER_FORMAT,
		},
		{
			errorMessage: 'Invalid code provided, please request a code again.',
			errorTranslationTypeIdentifier: CognitoErrorTypeTranslationIdentifier.REQUEST_CODE_AGAIN,
		},
	];

	constructor() {}

	public translateErrorMessage(message: string): CognitoErrorTypeTranslationIdentifier {
		// TODO: Remove this when all cognito errors are mapped
		console.log(message);
		const messageTranslationMatch: any = this.errorTranslations.find((cet: CognitoErrorTranslation) => {
			return cet.errorMessage === message;
		});

		if (!messageTranslationMatch) {
      return CognitoErrorTypeTranslationIdentifier.RESET_PASSWORD_GENERAL_ERROR
			console.error('unknown Cognito Error:', message);
		}

		return messageTranslationMatch.errorTranslationTypeIdentifier;
	}
}
