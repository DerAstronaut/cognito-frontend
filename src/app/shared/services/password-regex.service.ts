import {Injectable} from '@angular/core';

@Injectable()
export class PasswordRegexService {

    public regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])/gm;
    public numberRegex = /^(?=.*\d)/gm;
    public lowerCaseRegex = /^(?=.*[a-z])/gm;
    public upperCaseRegex = /(?=.*[A-Z])/gm;
    public specialCharacterRegex = /^(?=.*[$@$!%*?&])/gm;
    public minCharacterRegex = /[A-Za-z\d$@$!%*?&]{8,}/gm;

    constructor () {
    }
}
