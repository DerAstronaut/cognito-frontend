import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AmplifyService } from 'aws-amplify-angular';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	loggedIn$: BehaviorSubject<boolean> = new BehaviorSubject(false);

	constructor(private amplifyService: AmplifyService) {
		this.amplifyService.authStateChange$.subscribe(state => {
			if (state.user) {
				this.loggedIn$.next(true);
			} else {
				this.loggedIn$.next(false);
			}
		});
	}
}
