import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material/material.module';
import { CognitoErrorHandleService } from './services/cognito-error-handle.service';
import { UserService } from './services/user.service';

@NgModule({
	declarations: [],
	imports: [CommonModule, TranslateModule, MaterialModule],
	exports: [CommonModule, TranslateModule, MaterialModule],
	providers: [CognitoErrorHandleService, UserService],
	entryComponents: [],
})
export class SharedModule {}
