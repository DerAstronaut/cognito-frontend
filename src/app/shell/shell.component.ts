import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';
import { IosInstallComponent } from './../shared/components/ios-install/ios-install.component';
import { UserService } from '../shared/services/user.service';

@Component({
	selector: 'app-shell',
	templateUrl: './shell.component.html',
	styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {
	constructor(
		private breakpointObserver: BreakpointObserver,
		private toast: MatSnackBar,
		public userService: UserService
	) {}
	title = 'CognitoPWA';
	avatar: string;
	nav = [
		{
			title: 'S3 Test',
			translateKey: '',
			path: '/home',
		},
	];
	isHandset$: Observable<boolean> = this.breakpointObserver
		.observe(Breakpoints.Handset)
		.pipe(map(result => result.matches));

	ngOnInit(): void {
		this.userService.loggedIn$.subscribe(loggedIn => {
			if (loggedIn) {
				const index = this.nav.findIndex(item => {
					if (item.title === 'Registrieren') {
						return true;
					}
				});
				this.nav.splice(index, 1);
			} else {
				const index = this.nav.findIndex(item => {
					if (item.title === 'Registrieren') {
						return true;
					}
				});
				if (index <= -1) {
					this.nav.push({
						title: 'Registrieren',
						translateKey: '',
						path: '/auth/signin',
					});
				}
			}
		});
		this.detectIOS();
		this.checkSession();
	}

	async checkSession() {
		try {
			const userInfo = await Auth.currentUserInfo();
			if (userInfo && userInfo.attributes.profile) {
				const avatar = userInfo.attributes.profile;
				const url = (await Storage.vault.get(avatar)) as string;
				this.avatar = url;
			}
		} catch (error) {
			console.log('no session: ', error);
		}
	}

	detectIOS() {
		// Detects if device is on iOS
		const isIos = () => {
			const userAgent = window.navigator.userAgent.toLowerCase();
			return /iphone|ipad|ipod/.test(userAgent);
		};
		// Detects if device is in standalone mode
		const isInStandaloneMode = () => 'standalone' in (window as any).navigator && (window as any).navigator.standalone;

		// Checks if should display install popup notification:
		if (isIos() && !isInStandaloneMode()) {
			setTimeout(() => {
				this.toast.openFromComponent(IosInstallComponent, {
					duration: 8000,
					horizontalPosition: 'start',
					panelClass: ['mat-elevation-z3'],
				});
			});
		}
	}
}
